import './style.css';

// Exemplo com template string

let title: string = 'MeuApp';
let paragraph: string =
  'Lorem ipsum suscipit neque morbi sodales lectus molestie auctor, lacus scelerisque dui sed placerat quis.';

const appTitle: HTMLElement = document.getElementById('app.title');
const appParagraph: HTMLElement = document.getElementById('app.paragraph');

appTitle.innerHTML = title;
appParagraph.innerHTML = paragraph;
